/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_comb.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbeaufre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/02 12:13:50 by rbeaufre          #+#    #+#             */
/*   Updated: 2018/08/02 16:56:07 by rbeaufre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_putchar(char c);

void	printresult(int a1, int b1, int c1)
{
	if ((a1 == 7) && (b1 == 8) && (c1 == 9))
	{
		ft_putchar(a1 + '0');
		ft_putchar(b1 + '0');
		ft_putchar(c1 + '0');
	}
	else
	{
		ft_putchar(a1 + '0');
		ft_putchar(b1 + '0');
		ft_putchar(c1 + '0');
		ft_putchar(',');
		ft_putchar(' ');
	}
}

void	ft_print_comb(void)
{
	int		a;
	int		b;
	int		c;

	a = 0;
	while (a <= 7)
	{
		b = a + 1;
		while (b <= 8)
		{
			c = b + 1;
			while (c <= 9)
			{
				printresult(a, b, c);
				c = c + 1;
			}
			b = b + 1;
		}
		a = a + 1;
	}
}
