/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_comb2.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbeaufre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/02 12:42:07 by rbeaufre          #+#    #+#             */
/*   Updated: 2018/08/02 17:12:38 by rbeaufre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_putchar(char c);

void	printresult(int a, int b, int c, int d)
{
	if (((a * 10 + b) == 98) && ((c * 10 + d) == 99))
	{
		ft_putchar(a + '0');
		ft_putchar(b + '0');
		ft_putchar(' ');
		ft_putchar(c + '0');
		ft_putchar(d + '0');
	}
	else
	{
		ft_putchar(a + '0');
		ft_putchar(b + '0');
		ft_putchar(' ');
		ft_putchar(c + '0');
		ft_putchar(d + '0');
		ft_putchar(',');
		ft_putchar(' ');
	}
}

int		ft_print_comb2(void)
{
	int a;
	int b;

	a = 0;
	while (a <= 99)
	{
		b = a + 1;
		while (b <= 99)
		{
			printresult(a / 10, a % 10, b / 10, b % 10);
			b = b + 1;
		}
		a = a + 1;
	}
	return (0);
}
