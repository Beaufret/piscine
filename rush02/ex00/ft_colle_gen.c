/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_colle_gen.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbeaufre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/19 13:12:34 by rbeaufre          #+#    #+#             */
/*   Updated: 2018/08/19 22:31:25 by rbeaufre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft.h"

void		ft_addline(char *res, int x, char *line)
{
	int		i;
	int		j;

	i = 0;
	j = 0;
	while (res[j])
		j++;
	while (i < x)
	{
		if (i == 0)
			res[i + j] = line[0];
		else if (i == x - 1)
			res[i + j] = line[2];
		else
			res[i + j] = line[1];
		i++;
	}
	res[i + j] = '\n';
	res[i + j + 1] = '\0';
}

char		*ft_colle(int x, int y, t_colle colle)
{
	int		j;
	char	*res;

	res = (char*)malloc(sizeof(*res) * ((x * y) + 1));
	if (!res)
		return (0);
	j = 0;
	while (j < y)
	{
		if (j == 0)
			ft_addline(res, x, colle.sl);
		else if (j == y - 1)
			ft_addline(res, x, colle.el);
		else
			ft_addline(res, x, colle.ml);
		j++;
	}
	return (res);
}
