/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft.h                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbeaufre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/19 02:01:11 by rbeaufre          #+#    #+#             */
/*   Updated: 2018/08/19 23:12:03 by rbeaufre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_H
# define FT_H

# include <stdlib.h>
# include <unistd.h>

typedef struct	s_colle
{
	char *name;
	char *sl;
	char *ml;
	char *el;
}				t_colle;

int				ft_putnbr(int d);
void			ft_putstr(char *str);
void			ft_find_colle(char *str, char **t);
char			**ft_split(char *str, char *set);
void			ft_putchar(char c);
int				*ft_ij(char **tab);
char			*ft_colle(int x, int y, t_colle colle);
int				ft_strlen(char *str);
void			ft_free_split(char **tab);
char			*ft_strcpy(char *dest, char *src);

#endif
