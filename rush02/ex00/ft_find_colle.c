/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_find_colle.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbeaufre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/19 14:35:54 by rbeaufre          #+#    #+#             */
/*   Updated: 2018/08/19 23:26:04 by rbeaufre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./includes/ft.h"
#include "./includes/t_colle.h"

int		ft_strcmp(char *s1, char *s2)
{
	unsigned int i;

	i = 0;
	while (s1[i] != '\0' && s2[i] != '\0')
	{
		if (s1[i] != s2[i])
			return (s1[i] - s2[i]);
		i++;
	}
	if ((s1[i] == '\0' && s2[i] != '\0') || (s2[i] == '\0' && s1[i] != '\0'))
		return (s1[i] - s2[i]);
	return (0);
}

void	ft_print(char *name, int i, int j)
{
	ft_putstr(name);
	ft_putstr(" [");
	ft_putnbr(i);
	ft_putstr("] [");
	ft_putnbr(j);
	ft_putstr("]");
}

void	ft_bonus(int i, int j, int count)
{
	if (i > 0 && i == j)
	{
		if (count > 0)
			ft_putstr(" || ");
		ft_print("[carre]", i, j);
		count++;
	}
	if (i > 0)
	{
		if (count > 0)
			ft_putstr(" || ");
		ft_print("[rectangle]", i, j);
		count++;
	}
	if (count == 0)
		ft_putstr("aucune");
}

void	ft_find_colle(char *str, char **t)
{
	int		k;
	int		count;
	char	*tmp;

	k = 0;
	count = 0;
	while (k < 5)
	{
		tmp = ft_colle(ft_ij(t)[0], ft_ij(t)[1], g_colles[k]);
		if (ft_strcmp(str, tmp) == 0)
		{
			if (count > 0)
			{
				ft_putstr(" || ");
				ft_print(g_colles[k].name, ft_ij(t)[0], ft_ij(t)[1]);
			}
			else
				ft_print(g_colles[k].name, ft_ij(t)[0], ft_ij(t)[1]);
			count++;
		}
		k++;
	}
	ft_bonus(ft_ij(t)[0], ft_ij(t)[1], count);
	free(tmp);
	free(str);
}
