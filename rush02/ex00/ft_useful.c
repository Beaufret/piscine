/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_useful.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbeaufre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/18 15:12:39 by rbeaufre          #+#    #+#             */
/*   Updated: 2018/08/19 23:14:34 by rbeaufre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft.h"

void	ft_putchar(char c)
{
	write(1, &c, 1);
}

char	*ft_strcpy(char *dest, char *src)
{
	int i;

	i = -1;
	while (src[++i] != '\0')
		dest[i] = src[i];
	dest[i] = '\0';
	return (dest);
}

void	ft_putstr(char *str)
{
	int i;

	i = 0;
	while (str[i] != '\0')
	{
		ft_putchar(str[i]);
		i++;
	}
}

int		ft_strlen(char *str)
{
	int i;

	i = 0;
	while (str[i] != '\0')
		i++;
	return (i);
}

int		ft_putnbr(int d)
{
	if (d < 0 && d != -2147483648)
	{
		ft_putchar('-');
		d = d * -1;
	}
	if (d >= 10)
	{
		ft_putnbr(d / 10);
		ft_putchar(d % 10 + '0');
	}
	else
	{
		if (d == -2147483648)
		{
			ft_putchar('-');
			ft_putnbr(214748364);
			ft_putnbr(8);
		}
		else
		{
			ft_putchar(d + '0');
		}
	}
	return (0);
}
