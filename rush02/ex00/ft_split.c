/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_useful.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbeaufre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/18 15:06:53 by rbeaufre          #+#    #+#             */
/*   Updated: 2018/08/19 22:06:44 by rbeaufre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int		ft_sk_in_set(char c, char *set)
{
	int i;

	i = 0;
	while (set[i] != '\0')
	{
		if (set[i] == c)
			return (1);
		i++;
	}
	return (0);
}

int		*ft_count(char *s, int nmot, char *set)
{
	int		k;
	int		*tres;

	k = -5;
	if (!(tres = malloc(sizeof(int) * 4)))
		return (NULL);
	while (++k < 0)
		tres[k + 4] = 0;
	while (s[k] != '\0')
	{
		while (s[k] && ft_sk_in_set(s[k], set))
			k++;
		if (s[k] && !ft_sk_in_set(s[k], set))
			tres[1]++;
		tres[2] = k;
		while (s[k] && !ft_sk_in_set(s[k], set))
			k++;
		tres[3] += k - tres[2];
		tres[0] += k - tres[2];
		if (tres[1] == nmot)
			return (tres);
		tres[3] = 0;
	}
	return (tres);
}

char	*ft_print_word(char *str, int nmot, char *set)
{
	char	*res;
	int		k;

	k = 0;
	if (!(res = malloc(sizeof(char) * ft_count(str, nmot, set)[3])))
		return (NULL);
	while (k < ft_count(str, nmot, set)[3])
	{
		res[k] = str[ft_count(str, nmot, set)[2] + k];
		k++;
	}
	res[k] = '\0';
	return (res);
}

char	**ft_split(char *str, char *set)
{
	char	**res;
	int		i;

	if (!(res = malloc(sizeof(char*) * (ft_count(str, 0, set)[1]))))
		return (0);
	i = 0;
	while (i < ft_count(str, 0, set)[1])
	{
		if (!(res[i] = malloc(sizeof(char) * (ft_count(str, i + 1, set)[3]))))
			return (0);
		res[i] = ft_print_word(str, i + 1, set);
		i++;
	}
	res[i] = NULL;
	return (res);
}
