/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbeaufre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/18 16:52:39 by rbeaufre          #+#    #+#             */
/*   Updated: 2018/08/19 23:14:12 by rbeaufre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./includes/ft.h"

char	*ft_strcat(char *dest, char *src)
{
	int i;
	int j;

	i = 0;
	while (dest[i])
		i++;
	j = 0;
	while (src[j])
	{
		dest[i + j] = src[j];
		j++;
	}
	dest[i + j] = '\0';
	return (dest);
}

char	*ft_malloc(char *str, int j)
{
	if (!(str = malloc(sizeof(char) * j)))
		return (NULL);
	while (*str)
		*str = '\0';
	return (str);
}

char	*ft_read(void)
{
	char	buf[4097];
	char	*str;
	char	*temp;
	int		rt;
	int		strlen;

	if (!(str = (char *)malloc(sizeof(char))))
		return (0);
	*str = '\0';
	while ((rt = read(0, buf, 4096)))
	{
		temp = str;
		strlen = ft_strlen(str);
		str = ft_strcpy(ft_malloc(str, strlen + rt + 1), temp);
		if (str == NULL)
			return (NULL);
		buf[rt] = '\0';
		str = ft_strcat(str, buf);
	}
	return (str);
}

void	ft_free_split(char **tab)
{
	int i;

	i = 0;
	while (tab[i])
	{
		free(tab[i]);
		i++;
	}
	free(tab[i]);
	free(tab);
}

int		main(void)
{
	int		i;
	char	*str;
	char	**tmp;

	i = 0;
	str = ft_read();
	if (str[0] == '\0')
	{
		ft_putstr("aucune\n");
		return (0);
	}
	tmp = ft_split(str, "\n");
	ft_find_colle(str, tmp);
	free(tmp);
	ft_putstr("\n");
	return (0);
}
