/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_ij.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbeaufre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/19 13:22:00 by rbeaufre          #+#    #+#             */
/*   Updated: 2018/08/19 17:29:37 by rbeaufre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "./includes/ft.h"

int		*ft_ij(char **tab)
{
	int	*ij;
	int	a;
	int	b;
	int c;

	if (!(ij = malloc(sizeof(int) * 2)))
		return (NULL);
	a = 0;
	while (tab[a])
	{
		b = 0;
		while (tab[a][b])
			b++;
		a++;
		c = ((a == 1) ? b : c);
		if (c != b)
		{
			ij[0] = -1;
			ij[1] = -2;
			return (ij);
		}
	}
	ij[0] = b;
	ij[1] = a;
	return (ij);
}
