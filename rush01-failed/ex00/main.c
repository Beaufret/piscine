/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbeaufre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/12 11:38:42 by rbeaufre          #+#    #+#             */
/*   Updated: 2018/08/12 19:28:32 by rbeaufre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdlib.h>

void	ft_putchar(char c);

void	ft_print_tab(int **tab);

int		ft_error_arg(int argc, char **argv);

int		**ft_init_tab(int **tab, char **argv);

int		ft_line_ok(int **tab, int l, int k);

int		ft_col_ok(int **tab, int c, int k);

int		ft_sqr_ok(int **tab, int l, int c, int k);

int		ft_valid(int **tab, int pos);

void	ft_error(void);

int		main(int argc, char **argv)
{
	int **tab;
	int i;

	i = 0;
	tab = NULL;
	if (!(tab = malloc(sizeof(int*) * 9)))
		return (0);
	while (i < 9)
	{
		if (!(tab[i] = malloc(sizeof(int) * 9)))
			return (0);
		i++;
	}
	if (ft_error_arg(argc, argv) == 0)
	{
		ft_error();
		return (0);
	}
	tab = ft_init_tab(tab, argv + 1);
	if (ft_valid(tab, 0) == 0)
		ft_error();
	else
		ft_print_tab(tab);
	return (0);
}
