/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_valid.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbeaufre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/12 18:24:51 by rbeaufre          #+#    #+#             */
/*   Updated: 2018/08/12 19:25:00 by rbeaufre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_line_ok(int **tab, int l, int k);

int		ft_col_ok(int **tab, int c, int k);

int		ft_sqr_ok(int **tab, int l, int c, int k);

int		ft_valid(int **tab, int pos)
{
	int k;

	if (tab[pos / 9][pos % 9] != 0)
		return (ft_valid(tab, pos + 1));
	k = 1;
	while (k < 10)
	{
		if (ft_line_ok(tab, pos / 9, k) && ft_col_ok(tab, pos % 9, k) &&
				ft_sqr_ok(tab, pos / 9, pos % 9, k))
		{
			tab[pos / 9][pos % 9] = k;
			if (pos == 80)
				return (1);
			else if (ft_valid(tab, pos + 1) == 1)
				return (1);
			else
				tab[pos / 9][pos % 9] = 0;
		}
		else
			tab[pos / 9][pos % 9] = 0;
		k++;
	}
	return (0);
}
