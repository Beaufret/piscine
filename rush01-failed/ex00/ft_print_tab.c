/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_tab.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbeaufre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/12 14:59:03 by rbeaufre          #+#    #+#             */
/*   Updated: 2018/08/12 18:30:00 by rbeaufre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_putchar(char c);

void	ft_print_tab(int **tab)
{
	int l;
	int c;

	l = 0;
	while (l < 9)
	{
		c = 0;
		while (c < 9)
		{
			ft_putchar(tab[l][c] + '0');
			if (c != 8)
				ft_putchar(' ');
			c++;
		}
		ft_putchar('\n');
		l++;
	}
}
