/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_error_arg.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbeaufre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/12 15:00:54 by rbeaufre          #+#    #+#             */
/*   Updated: 2018/08/12 15:01:17 by rbeaufre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_error_arg(int argc, char **argv)
{
	int i;
	int j;

	if (argc != 10)
		return (0);
	i = 1;
	while (i < 10)
	{
		j = 0;
		while (argv[i][j])
			j++;
		if (j != 9)
			return (0);
		j = 0;
		while (j < 9)
		{
			if (argv[i][j] != 46 &&
					!(argv[i][j] >= 49 && argv[i][j] <= 57))
				return (0);
			j++;
		}
		i++;
	}
	return (1);
}
