/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_functions.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbeaufre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/16 14:15:20 by rbeaufre          #+#    #+#             */
/*   Updated: 2018/08/16 17:33:06 by rbeaufre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft.h"

void		ft_add(int n1, int n2)
{
	ft_putnbr(n1 + n2);
}

void		ft_sub(int n1, int n2)
{
	ft_putnbr(n1 - n2);
}

void		ft_mul(int n1, int n2)
{
	ft_putnbr(n1 * n2);
}

void		ft_div(int n1, int n2)
{
	if (n2 == 0)
		ft_putstr("Stop : division by zero");
	else
		ft_putnbr(n1 / n2);
}

void		ft_mod(int n1, int n2)
{
	if (n2 == 0)
		ft_putstr("Stop : modulo by zero");
	else
		ft_putnbr(n1 % n2);
}
