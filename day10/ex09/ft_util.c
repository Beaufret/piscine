/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_util.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbeaufre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/16 21:16:41 by rbeaufre          #+#    #+#             */
/*   Updated: 2018/08/16 21:39:49 by rbeaufre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft.h"

int		ft_strcmp(char *s1, char *s2)
{
	int i;

	i = 0;
	while (s1[i] == s2[i] && s1[i] != '\0' && s2[i] != '\0')
		i++;
	return (s1[i] - s2[i]);
}

int		ft_is_number(char c)
{
	if (c <= 57 && c >= 48)
		return (1);
	else
		return (0);
}

int		ft_atoi(char *str)
{
	int	sgn;
	int i;
	int res;

	res = 0;
	i = 0;
	sgn = 1;
	while (str[i] == 32 || (str[i] <= 13 && str[i] >= 9))
		i++;
	if (str[i] == '-')
	{
		sgn = -1;
		i++;
	}
	else if (str[i] == '+')
		i = i + 1;
	while (ft_is_number(str[i]) == 1)
	{
		res = res * 10 + (str[i] - '0');
		i++;
	}
	return (sgn * res);
}
