/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbeaufre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/16 23:47:01 by rbeaufre          #+#    #+#             */
/*   Updated: 2018/08/17 13:47:22 by rbeaufre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft.h"
#include "ft_opp.h"

void	ft_usage(int n1, int n2)
{
	unsigned long i;

	n2 = n1 - n2 + n2;
	i = 0;
	ft_putstr("error : only [ ");
	while (g_opptab[i].op[0] != '\0')
	{
		ft_putstr(g_opptab[i].op);
		ft_putchar(' ');
		i++;
	}
	ft_putstr("] are accepted.\n");
}

int		ft_op_valid(char *c)
{
	int i;

	i = 0;
	while (g_opptab[i].op[0] != '\0')
	{
		if (ft_strcmp(c, g_opptab[i].op) == 0)
		{
			return (1);
		}
		i++;
	}
	ft_usage(0, 0);
	return (0);
}

int		main(int argc, char **argv)
{
	int l;
	int i;

	if (argc != 4)
		return (0);
	l = ft_strlen(argv[2]);
	if (!(ft_op_valid(argv[2])))
	{
		return (0);
	}
	i = 0;
	while (i < 5)
	{
		if (ft_strcmp(argv[2], g_opptab[i].op) == 0)
		{
			g_opptab[i].f(ft_atoi(argv[1]), ft_atoi(argv[3]));
		}
		i++;
	}
	ft_putchar('\n');
	return (0);
}
