/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   do-op.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbeaufre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/16 13:53:22 by rbeaufre          #+#    #+#             */
/*   Updated: 2018/08/16 17:51:21 by rbeaufre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft.h"
#include "ft_opp.h"

int		ft_is_number(char c)
{
	if (c <= 57 && c >= 48)
		return (1);
	else
		return (0);
}

int		ft_atoi(char *str)
{
	int	sgn;
	int i;
	int res;

	res = 0;
	i = 0;
	sgn = 1;
	while (str[i] == 32 || (str[i] <= 13 && str[i] >= 9))
		i++;
	if (str[i] == '-')
	{
		sgn = -1;
		i++;
	}
	else if (str[i] == '+')
		i = i + 1;
	while (ft_is_number(str[i]) == 1)
	{
		res = res * 10 + (str[i] - '0');
		i++;
	}
	return (sgn * res);
}

int		ft_op_valid(char *c)
{
	if (c[1] != '\0')
		return (0);
	if (c[0] == '+' || c[0] == '-' || c[0] == '*' || c[0] == '/' ||
			c[0] == '%')
		return (1);
	return (0);
}

int		main(int argc, char **argv)
{
	int l;
	int i;

	if (argc != 4)
		return (0);
	l = ft_strlen(argv[2]);
	if (!(ft_op_valid(argv[2])))
	{
		ft_putchar('0');
		ft_putchar('\n');
		return (0);
	}
	i = 0;
	while (i < 5)
	{
		if (argv[2][0] == g_opptab[i].op[0])
		{
			g_opptab[i].f(ft_atoi(argv[1]), ft_atoi(argv[3]));
		}
		i++;
	}
	ft_putchar('\n');
	return (0);
}
