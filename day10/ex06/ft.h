/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft.h                                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbeaufre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/16 12:35:30 by rbeaufre          #+#    #+#             */
/*   Updated: 2018/08/16 17:37:38 by rbeaufre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_H
# define FT_H

# include <stdlib.h>
# include <stdio.h>
# include <unistd.h>

typedef struct		s_opp
{
	char			*op;
	void			(*f)(int, int);
}					t_opp;

void				ft_putstr(char *str);
int					ft_putnbr(int i);
void				ft_putchar(char c);
int					ft_strlen(char *str);
void				ft_add(int n1, int n2);
void				ft_sub(int n1, int n2);
void				ft_mul(int n1, int n2);
void				ft_div(int n1, int n2);
void				ft_mod(int n1, int n2);

#endif
