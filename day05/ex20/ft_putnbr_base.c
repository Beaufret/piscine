/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_putnbr_base.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbeaufre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/08 19:31:36 by rbeaufre          #+#    #+#             */
/*   Updated: 2018/08/08 19:31:41 by rbeaufre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_putchar(char c);

int		ft_base_ok(char *base)
{
	int i;
	int j;

	i = 0;
	while (base[i])
	{
		j = 0;
		while (base[j])
		{
			if (base[i] == base[j] && i != j)
				return (0);
			if (base[i] == '+' || base[i] == '-')
				return (0);
			j++;
		}
		i++;
	}
	if (j == 0 || j == 1)
		return (0);
	return (i);
}

void	ft_putnbr_base(int nb, char *base)
{
	int i;
	int overflow;

	if (ft_base_ok(base) == 0)
		return ;
	i = 0;
	if (nb < 0)
	{
		ft_putchar('-');
		if (nb == -2147483648)
		{
			overflow = 2147483648 % ft_base_ok(base);
			ft_putnbr_base(2147483648 / ft_base_ok(base), base);
			ft_putchar(base[overflow]);
		}
		else
			ft_putnbr_base(nb * -1, base);
	}
	else if (nb >= 0 && nb < ft_base_ok(base))
		ft_putchar(base[nb]);
	else
	{
		ft_putnbr_base(nb / ft_base_ok(base), base);
		ft_putnbr_base(nb % ft_base_ok(base), base);
	}
}
