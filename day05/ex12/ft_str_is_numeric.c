/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_is_numeric.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbeaufre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/08 11:06:47 by rbeaufre          #+#    #+#             */
/*   Updated: 2018/08/08 12:19:00 by rbeaufre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_str_is_numeric(char *str)
{
	int		i;
	char	x;
	char	y;

	x = str[0];
	if (str[0] == '\0')
		return (1);
	y = str[1];
	if (y == '\0' && (((x >= '0') && (x < '9'))))
		return (1);
	i = 0;
	while (str[i] != '\0')
	{
		if (str[i] >= '0' && str[i] <= '9')
		{
			return (ft_str_is_numeric(str + 1));
		}
		else
			return (0);
		i++;
	}
	return (0);
}
