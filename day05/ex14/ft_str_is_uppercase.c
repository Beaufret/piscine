/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_is_uppercase.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbeaufre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/08 12:24:05 by rbeaufre          #+#    #+#             */
/*   Updated: 2018/08/08 12:25:30 by rbeaufre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_str_is_uppercase(char *str)
{
	int		i;
	char	x;
	char	y;

	x = str[0];
	if (str[0] == '\0')
		return (1);
	y = str[1];
	if (y == '\0' && (((x >= 'A') && (x < 'Z'))))
		return (1);
	i = 0;
	while (str[i] != '\0')
	{
		if (str[i] >= 'A' && str[i] <= 'Z')
		{
			return (ft_str_is_uppercase(str + 1));
		}
		else
			return (0);
		i++;
	}
	return (0);
}
