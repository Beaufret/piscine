/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_is_lowercase.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbeaufre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/08 12:20:38 by rbeaufre          #+#    #+#             */
/*   Updated: 2018/08/08 12:23:27 by rbeaufre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_str_is_lowercase(char *str)
{
	int		i;
	char	x;
	char	y;

	x = str[0];
	if (str[0] == '\0')
		return (1);
	y = str[1];
	if (y == '\0' && (((x >= 'a') && (x < 'z'))))
		return (1);
	i = 0;
	while (str[i] != '\0')
	{
		if (str[i] >= 'a' && str[i] <= 'z')
		{
			return (ft_str_is_lowercase(str + 1));
		}
		else
			return (0);
		i++;
	}
	return (0);
}
