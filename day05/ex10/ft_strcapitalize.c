/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcapitalize.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbeaufre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/08 01:54:36 by rbeaufre          #+#    #+#             */
/*   Updated: 2018/08/09 00:06:20 by rbeaufre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_word_beginning(char *str, int i)
{
	int x;
	int y;

	x = str[i - 1];
	y = str[i];
	if ((y >= 'a' && y <= 'z') || (y >= 'A' && y <= 'Z'))
	{
		if ((x < 48 || x > 57) && (x < 97 || x > 122) && (x < 65 || x > 90))
			return (1);
	}
	return (0);
}

char	*ft_strcapitalize(char *str)
{
	unsigned int i;

	if (str[0] != '\0' && str[0] >= 'a' && str[0] <= 'z')
		str[0] -= 32;
	i = 1;
	while (str[i] != '\0')
	{
		if (ft_word_beginning(str, i) == 1)
		{
			if (str[i] >= 'a' && str[i] <= 'z')
				str[i] -= 32;
		}
		else if (str[i] >= 'A' && str[i] <= 'Z')
			str[i] += 32;
		++i;
	}
	return (str);
}
