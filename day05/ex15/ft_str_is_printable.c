/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_str_is_printable.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbeaufre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/08 12:26:39 by rbeaufre          #+#    #+#             */
/*   Updated: 2018/08/08 17:20:28 by rbeaufre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_str_is_printable(char *str)
{
	int		i;
	char	x;
	char	y;

	x = str[0];
	if (str[0] == '\0')
		return (1);
	y = str[1];
	if (y == '\0' && x > 31 && x != 127)
		return (1);
	i = 0;
	while (str[i] != '\0')
	{
		if (str[i] > 31 && x != 127)
		{
			return (ft_str_is_printable(str + 1));
		}
		else
			return (0);
		i++;
	}
	return (0);
}
