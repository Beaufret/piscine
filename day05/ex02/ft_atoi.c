/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_atoi.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbeaufre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/05 18:11:42 by rbeaufre          #+#    #+#             */
/*   Updated: 2018/08/05 18:41:24 by rbeaufre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		isnumber(char c)
{
	if (c <= 57 && c >= 48)
		return (1);
	else
		return (0);
}

int		ft_atoi(char *str)
{
	int	sgn;
	int i;
	int res;

	res = 0;
	i = 0;
	sgn = 1;
	while (str[i] == 32 || (str[i] <= 13 && str[i] >= 9))
		i++;
	if (str[i] == '-')
	{
		sgn = -1;
		i++;
	}
	else if (str[i] == '+')
		i = i + 1;
	while (isnumber(str[i]) == 1)
	{
		res = res * 10 + (str[i] - '0');
		i++;
	}
	return (sgn * res);
}
