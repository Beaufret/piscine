/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_spy.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbeaufre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/10 11:29:14 by rbeaufre          #+#    #+#             */
/*   Updated: 2018/08/10 11:34:44 by rbeaufre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdio.h>

int		is_first(char *str, int pos)
{
	int res;

	res = 0;
	if (pos == 0)
		res = 1;
	else if (str[pos - 1] < 48 ||
			(str[pos - 1] > 57 && str[pos - 1] < 65) ||
			(str[pos - 1] > 90 && str[pos - 1] < 97) ||
			str[pos - 1] > 122)
		res = 1;
	return (res);
}

char	*ft_strcapitalize(char *str)
{
	int		i;

	i = 0;
	while (str[i] != 0)
	{
		if (is_first(str, i) == 1 && str[i] >= 97 && str[i] <= 122)
			str[i] = str[i] - 32;
		else if (is_first(str, i) == 1)
			str[i] = str[i];
		else if (str[i] >= 65 && str[i] <= 90)
			str[i] = str[i] + 32;
		i++;
	}
	return (str);
}

char	*ft_find_letters(char *str)
{
	int		i;
	int		j;
	int		s;
	char	res_i[100];
	char	*res;

	i = 0;
	j = 0;
	s = 1;
	res = res_i;
	while (str[i] && s == 1)
	{
		if ((str[i] >= 'A' && str[i] <= 'Z') ||
				(str[i] >= 'a' && str[i] <= 'z'))
		{
			res_i[j] = str[i];
			j++;
		}
		else if (j != 0)
			s = 0;
		i++;
	}
	res[j] = '\0';
	return (res);
}

int		ft_strcmp(char *s1, char *s2)
{
	int index;
	int res;

	index = 0;
	res = 0;
	while (s1[index] == s2[index] && s1[index] != '\0')
		index++;
	return (s1[index] - s2[index]);
}

int		main(int argc, char **argv)
{
	int		i;

	if (argc >= 1)
	{
		i = 1;
		while (i < argc)
		{
			if (ft_strcmp(ft_strcapitalize(ft_find_letters(argv[i])),
						"Bauer") == 0 ||
				ft_strcmp(ft_strcapitalize(ft_find_letters(argv[i])),
						"President") == 0 ||
				ft_strcmp(ft_strcapitalize(ft_find_letters(argv[i])),
						"Attack") == 0)
				write(1, "Alert!!!", 8);
			i++;
		}
	}
	return (0);
}
