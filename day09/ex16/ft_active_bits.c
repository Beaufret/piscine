/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_active_bits.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbeaufre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/10 14:26:32 by rbeaufre          #+#    #+#             */
/*   Updated: 2018/08/10 14:38:56 by rbeaufre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

unsigned int	ft_active_bits(int value)
{
	unsigned int res;

	res = 0;
	while (value != 0)
	{
		res += (value % 2);
		value /= 2;
	}
	return (res);
}
