/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_antidote.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbeaufre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/09 22:29:28 by rbeaufre          #+#    #+#             */
/*   Updated: 2018/08/09 22:42:32 by rbeaufre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	sort_integer_table(int *tab, int size)
{
	int count;
	int i;
	int tmp;

	count = 0;
	while (count < size)
	{
		i = 0;
		while (i < (size - 1))
		{
			if (tab[i] > tab[i + 1])
			{
				tmp = tab[i];
				tab[i] = tab[i + 1];
				tab[i + 1] = tmp;
			}
			i++;
		}
		count++;
	}
}

int		ft_antidote(int i, int j, int k)
{
	int		tab[3];

	tab[0] = i;
	tab[1] = j;
	tab[2] = k;
	sort_integer_table(tab, 3);
	return (tab[1]);
}
