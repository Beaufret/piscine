/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_takes_place.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbeaufre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/09 17:56:23 by rbeaufre          #+#    #+#             */
/*   Updated: 2018/08/09 18:44:12 by rbeaufre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdio.h>

int		ft_form(int hour)
{
	if (hour >= 12 && hour <= 24)
	{
		hour -= (hour == 12) ? 0 : 12;
		return (hour);
	}
	else if (hour >= 0 && hour < 12)
	{
		if (hour == 0)
			return (hour + 12);
		else
			return (hour);
	}
	else
		return (99);
}

char	*ft_m(int hour)
{
	if (hour >= 12 && hour < 24)
		return ("P.M.");
	if (hour < 12 || hour == 24)
		return ("A.M.");
	return ("ampm_bug");
}

void	ft_takes_place(int h)
{
	char	*format;

	format = "THE FOLLOWING TAKES PLACE BETWEEN %d.00 %s AND %d.00 %s\n";
	printf(format, ft_form(h), ft_m(h), ft_form(h + 1), ft_m(h + 1));
}
