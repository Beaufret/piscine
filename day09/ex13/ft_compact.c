/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_compact.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbeaufre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/10 13:59:11 by rbeaufre          #+#    #+#             */
/*   Updated: 2018/08/10 14:09:37 by rbeaufre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_empty(char **tab, int i, int length)
{
	while (i + 1 < length)
	{
		tab[i] = tab[i + 1];
		i++;
	}
}

int		ft_compact(char **tab, int length)
{
	int i;

	i = 0;
	while (i < length)
	{
		if (tab[i] == 0)
		{
			ft_empty(tab, i, length);
			i = 0;
			length--;
		}
		else
			i++;
	}
	return (length);
}
