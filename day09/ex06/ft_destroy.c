/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_destroy.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbeaufre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/10 08:52:02 by rbeaufre          #+#    #+#             */
/*   Updated: 2018/08/10 09:05:46 by rbeaufre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "ft_ultimator.h"

void	ft_destroy(char ***factory)
{
	int i;
	int x;

	if (factory == 0)
		return ;
	i = 0;
	while (factory[i] != 0)
	{
		x = 0;
		while (factory[i][x] != 0)
		{
			free(factory[i][x]);
			x++;
		}
		free(factory[i]);
		i++;
	}
	free(factory);
}
