/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_collatz_conjecture.c                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbeaufre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/10 09:05:58 by rbeaufre          #+#    #+#             */
/*   Updated: 2018/08/10 12:17:02 by rbeaufre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int				ft_is_odd(unsigned int base)
{
	if (base % 2 == 0)
		return (1);
	else
		return (0);
}

unsigned int	ft_collatz_conjecture(unsigned int base)
{
	unsigned int res;

	res = 0;
	if (base != 1 && base != 0)
	{
		if (ft_is_odd(base) == 1)
			res = res + ft_collatz_conjecture(base / 2);
		else
			res = res + ft_collatz_conjecture((base * 3) + 1);
		res++;
	}
	return (res);
}
