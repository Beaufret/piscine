/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_door.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbeaufre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/10 12:48:29 by rbeaufre          #+#    #+#             */
/*   Updated: 2018/08/10 13:56:22 by rbeaufre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_DOOR_H
# define FT_DOOR_H
# include <unistd.h>
# define TRUE (1)
# define FALSE (0)
# define OPEN (1)
# define CLOSE (0)

typedef int		t_bool;

typedef struct	s_door
{
	t_bool state;
}				t_door;

#endif
