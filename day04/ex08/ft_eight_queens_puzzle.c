/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_eight_queens_puzzle.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbeaufre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/06 17:58:58 by rbeaufre          #+#    #+#             */
/*   Updated: 2018/08/06 22:28:35 by rbeaufre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		abs(int value)
{
	return (value >= 0 ? value : -value);
}

int		position_ok(char *tab, int y)
{
	int x;

	x = 0;
	while (x < y)
	{
		if (tab[x] == tab[y])
			return (0);
		if (abs(tab[x] - tab[y]) == abs(x - y))
			return (0);
		x++;
	}
	return (1);
}

void	place(char *tab, int y, int *res)
{
	tab[y] = '0';
	while (tab[y] <= '7')
	{
		if (position_ok(tab, y))
		{
			if (y == 7)
				*res += 1;
			else
				place(tab, y + 1, res);
		}
		tab[y]++;
	}
}

int		ft_eight_queens_puzzle(void)
{
	char	tab[9];
	int		res;

	res = 0;
	tab[8] = '\0';
	place(tab, 0, &res);
	return (res);
}
