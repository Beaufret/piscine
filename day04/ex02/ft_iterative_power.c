/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_iterative_power.c                               :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbeaufre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/06 12:51:52 by rbeaufre          #+#    #+#             */
/*   Updated: 2018/08/06 13:58:24 by rbeaufre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_iterative_power(int nb, int power)
{
	int i;
	int res;

	if (power == 0)
		return (1);
	if (power <= 0)
		return (0);
	i = 1;
	res = 1;
	while (i++ <= power)
	{
		res = res * nb;
	}
	return (res);
}
