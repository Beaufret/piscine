/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_eight_queens_puzzle.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbeaufre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/06 17:58:58 by rbeaufre          #+#    #+#             */
/*   Updated: 2018/08/06 23:35:44 by rbeaufre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_putchar(char c);

int		abs(int value)
{
	return (value >= 0 ? value : -value);
}

int		position_ok(int *tab, int y)
{
	int x;

	x = 0;
	while (x < y)
	{
		if (tab[x] == tab[y])
			return (0);
		if (abs(tab[x] - tab[y]) == abs(x - y))
			return (0);
		x++;
	}
	return (1);
}

void	place(int *tab, int y, int *res)
{
	int k;

	tab[y] = 0;
	while (tab[y] <= 7)
	{
		if (position_ok(tab, y))
		{
			if (y == 7)
			{
				*res += 1;
				k = 0;
				while (k <= 7)
				{
					ft_putchar(tab[k] + 1 + '0');
					k++;
				}
				ft_putchar('\n');
			}
			else
				place(tab, y + 1, res);
		}
		tab[y]++;
	}
}

void	ft_eight_queens_puzzle_2(void)
{
	int		tab[9];
	int		res;

	res = 0;
	tab[8] = '\0';
	place(tab, 0, &res);
}
