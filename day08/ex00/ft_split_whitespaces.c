/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_split_whitespaces.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbeaufre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/13 15:26:05 by rbeaufre          #+#    #+#             */
/*   Updated: 2018/08/14 09:40:21 by rbeaufre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int		*ft_count(char *s, int nmot)
{
	int k;
	int *tres;

	k = -5;
	if (!(tres = malloc(sizeof(int) * 4)))
		return (NULL);
	while (++k < 0)
		tres[k + 4] = 0;
	while (s[k] != '\0')
	{
		while (s[k] && (s[k] == ' ' || s[k] == '\t' || s[k] == '\n'))
			k++;
		if (s[k] && !(s[k] == ' ' || s[k] == '\t' || s[k] == '\n'))
			tres[1]++;
		tres[2] = k;
		while (s[k] && !(s[k] == ' ' || s[k] == '\t' || s[k] == '\n'))
			k++;
		tres[3] += k - tres[2];
		tres[0] += k - tres[2];
		if (tres[1] == nmot)
			return (tres);
		tres[3] = 0;
	}
	return (tres);
}

char	*ft_print_word(char *str, int nmot)
{
	char	*res;
	int		k;

	k = 0;
	if (!(res = malloc(sizeof(char) * ft_count(str, nmot)[3])))
		return (NULL);
	while (k < ft_count(str, nmot)[3])
	{
		res[k] = str[ft_count(str, nmot)[2] + k];
		k++;
	}
	res[k] = '\0';
	return (res);
}

char	**ft_split_whitespaces(char *str)
{
	char	**res;
	int		i;

	if (!(res = malloc(sizeof(char*) * (ft_count(str, 0)[1]))))
		return (0);
	i = 0;
	while (i < ft_count(str, 0)[1])
	{
		if (!(res[i] = malloc(sizeof(char) * (ft_count(str, i + 1)[3]))))
			return (0);
		res[i] = ft_print_word(str, i + 1);
		i++;
	}
	res[i] = NULL;
	return (res);
}
