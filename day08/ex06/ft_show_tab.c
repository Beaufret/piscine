/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_show_tab.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbeaufre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/15 12:10:20 by rbeaufre          #+#    #+#             */
/*   Updated: 2018/08/15 17:29:54 by rbeaufre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_stock_par.h"

int		ft_putnbr(int d)
{
	if (d < 0 && d != -2147483648)
	{
		ft_putchar('-');
		d = d * -1;
	}
	if (d >= 10)
	{
		ft_putnbr(d / 10);
		ft_putchar(d % 10 + '0');
	}
	else
	{
		if (d == -2147483648)
		{
			ft_putchar('-');
			ft_putnbr(214748364);
			ft_putnbr(8);
		}
		else
		{
			ft_putchar(d + '0');
		}
	}
	return (0);
}

void	ft_show_tab(struct s_stock_par *par)
{
	int k;
	int l;
	int m;

	k = 0;
	if (par == NULL)
		return ;
	while (par[k].str)
	{
		l = 0;
		while (par[k].str[l])
			ft_putchar(par[k].str[l++]);
		ft_putchar('\n');
		ft_putnbr(par[k].size_param);
		ft_putchar('\n');
		m = -1;
		while (par[k].tab[++m] != 0)
		{
			l = 0;
			while (par[k].tab[m][l] != '\0')
				ft_putchar(par[k].tab[m][l++]);
			ft_putchar('\n');
		}
		k++;
	}
}
