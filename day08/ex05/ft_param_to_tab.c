/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_param_to_tab.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbeaufre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/15 10:36:25 by rbeaufre          #+#    #+#             */
/*   Updated: 2018/08/16 11:51:10 by rbeaufre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "ft_stock_par.h"

int						ft_strlen(char *str)
{
	int		i;

	i = 0;
	while (str[i])
		i++;
	return (i);
}

char					*ft_strdup(char *src)
{
	int		i;
	char	*res;

	i = 0;
	res = (char*)malloc(sizeof(*src) * (ft_strlen(src) + 1));
	if (!res)
		return (NULL);
	while (src[i])
	{
		res[i] = src[i];
		i++;
	}
	res[i] = '\0';
	return (res);
}

struct s_stock_par		ft_set_param(struct s_stock_par *s, char *param)
{
	s->copy = ft_strdup(param);
	s->size_param = ft_strlen(param);
	s->str = param;
	s->tab = ft_split_whitespaces(param);
	return (*s);
}

struct s_stock_par		*ft_param_to_tab(int ac, char **av)
{
	int						i;
	struct s_stock_par		*res;

	i = 0;
	res = (struct s_stock_par*)malloc(sizeof(struct s_stock_par) * (ac + 1));
	while (i < ac)
	{
		res[i] = ft_set_param(&res[i], av[i]);
		i++;
	}
	res[i].str = 0;
	return (res);
}
