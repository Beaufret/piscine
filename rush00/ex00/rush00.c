/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rush00.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbeaufre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/04 09:04:01 by rbeaufre          #+#    #+#             */
/*   Updated: 2018/08/04 11:09:39 by rbeaufre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_putchar(char c);

void	affichelignenormale(int x)
{
	int		i;

	if (x > 0)
	{
		ft_putchar('|');
		i = 1;
		while (i < x - 1)
		{
			ft_putchar(' ');
			i++;
		}
		if (x != 1)
			ft_putchar('|');
		ft_putchar('\n');
	}
}

void	affichelignenonnormale(int x)
{
	int		i;

	if (x > 0)
	{
		ft_putchar('o');
		i = 1;
		while (i < x - 1)
		{
			ft_putchar('-');
			i++;
		}
		if (x != 1)
			ft_putchar('o');
		ft_putchar('\n');
	}
}

void	rush(int x, int y)
{
	int		j;

	j = 0;
	if (y > 0)
	{
		while (j < y)
		{
			if ((j == 0) || (j == y - 1))
				affichelignenonnormale(x);
			else
				affichelignenormale(x);
			j++;
		}
	}
}
