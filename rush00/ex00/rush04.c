/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   rush02.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbeaufre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/04 09:04:01 by rbeaufre          #+#    #+#             */
/*   Updated: 2018/08/04 14:24:24 by rbeaufre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_putchar(char c);

void	affichelignedebut(int x)
{
	int		i;

	if (x > 0)
	{
		ft_putchar('A');
		i = 1;
		while (i < x - 1)
		{
			ft_putchar('B');
			i++;
		}
		if (x != 1)
			ft_putchar('C');
		ft_putchar('\n');
	}
}

void	affichelignenormale(int x)
{
	int		i;

	if (x > 0)
	{
		ft_putchar('B');
		i = 1;
		while (i < x - 1)
		{
			ft_putchar(' ');
			i++;
		}
		if (x != 1)
			ft_putchar('B');
		ft_putchar('\n');
	}
}

void	affichelignefin(int x)
{
	int		i;

	if (x > 0)
	{
		ft_putchar('C');
		i = 1;
		while (i < x - 1)
		{
			ft_putchar('B');
			i++;
		}
		if (x != 1)
			ft_putchar('A');
		ft_putchar('\n');
	}
}

void	rush(int x, int y)
{
	int		j;

	j = 0;
	if (y > 0)
	{
		while (j < y)
		{
			if (j == 0)
				affichelignedebut(x);
			else if (j == y - 1)
				affichelignefin(x);
			else
				affichelignenormale(x);
			j++;
		}
	}
}
