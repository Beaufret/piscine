/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_convert_base.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbeaufre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/13 19:08:25 by rbeaufre          #+#    #+#             */
/*   Updated: 2018/08/14 14:41:45 by rbeaufre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include <stdio.h>

int		ft_base_screen(char *base)
{
	int i;
	int j;

	i = 0;
	while (base[i])
	{
		if (base[i] == '+' || base[i] == '-')
			return (0);
		j = 0;
		while (base[j])
		{
			if (i != j && base[i] == base[j])
				return (0);
			j++;
		}
		i++;
	}
	return (i);
}

int		ft_get_char_value(char c, char *base)
{
	int i;

	i = 0;
	while (base[i])
	{
		if (base[i] == c)
			return (i);
		i++;
	}
	return (0);
}

char	*ft_strrev(char *str)
{
	int		a;
	int		i;
	char	c;

	a = 0;
	while (str[a] != '\0')
		a++;
	a = a - 1;
	i = 0;
	while (i < ((a + 1) / 2))
	{
		c = str[i];
		str[i] = str[a - i];
		str[a - i] = c;
		i++;
	}
	return (str);
}

char	*ft_write_nbr_base(int nb, char *base_to)
{
	char	*nbr;
	int		i;
	int		nb2;
	int		size_nb;

	nb2 = nb;
	i = (nb == 0) ? 1 : 0;
	while (nb2 > 0)
	{
		nb2 = nb2 / ft_base_screen(base_to);
		i++;
	}
	nb2 = nb;
	size_nb = i;
	if (!(nbr = malloc(sizeof(char) * (i + 1))))
		return (NULL);
	i = 0;
	while (i < size_nb)
	{
		nbr[i++] = base_to[nb2 % ft_base_screen(base_to)];
		nb2 = nb2 / ft_base_screen(base_to);
	}
	nbr[i] = '\0';
	return (ft_strrev(nbr));
}

char	*ft_convert_base(char *nbr, char *base_from, char *base_to)
{
	int base_len;
	int nb;
	int i;
	int sgn;

	i = 0;
	sgn = 1;
	nb = 0;
	if (ft_base_screen(base_from) == 0 || ft_base_screen(base_to) == 0)
		return (0);
	base_len = ft_base_screen(base_from);
	while (nbr[i] != '\0')
	{
		if (nbr[i] == '-')
			return (0);
		nb = nb * base_len + ft_get_char_value(nbr[i], base_from);
		i++;
	}
	return (ft_write_nbr_base(nb, base_to));
}
