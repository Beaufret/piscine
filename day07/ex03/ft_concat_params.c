/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_concat_params.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbeaufre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/13 14:36:18 by rbeaufre          #+#    #+#             */
/*   Updated: 2018/08/14 13:16:50 by rbeaufre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int		ft_strlen(char *str)
{
	int i;

	i = 0;
	while (str[i])
		i++;
	return (i);
}

int		ft_argvlen(char **argv)
{
	int i;
	int res;

	i = 1;
	res = 0;
	while (argv[i])
	{
		res += ft_strlen(argv[i]);
		i++;
	}
	return (res);
}

char	*ft_concat_params(int argc, char **argv)
{
	char	*res;
	int		i;
	int		j;
	int		k;

	k = 0;
	if (argc == 1)
		return ("");
	if (!(res = malloc(sizeof(char) * (ft_argvlen(argv) + 1))))
		return (0);
	i = 1;
	while (i < argc)
	{
		j = 0;
		while (argv[i][j] != 0)
			res[k++] = argv[i][j++];
		if (i != argc - 1)
			res[k++] = '\n';
		i++;
	}
	res[k] = '\0';
	return (res);
}
