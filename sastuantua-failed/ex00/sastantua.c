/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   sastantua.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rbeaufre <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/08/05 09:21:05 by rbeaufre          #+#    #+#             */
/*   Updated: 2018/08/05 16:06:41 by rbeaufre         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <unistd.h>
#include <stdio.h>

int		donne_largeur(int etage)
{
	int		u;

	if (etage == 0)
		return (7);
	else
	{
		u = ((1 + (etage / 2) + (etage % 2) + (etage + 3)));
		return (donne_largeur(etage - 1) + (2 * u));
	}
}

void	affiche_etage(int hauteur, int largeur, int decal)
{
	int		i;
	int		j;

	j = 0;
	while (j < hauteur)
	{
		i = -1 - decal;
		while (i++ < hauteur - 2 - j)
			write(1, " ", 1);
		write(1, "/", 1);
		i--;
		while (i++ < (largeur + j - hauteur - 2))
			write(1, "*", 1);
		write(1, "\\", 1);
		write(1, "\n", 1);
		j++;
	}
}

void	ecrire(int l, int s, int j, int i)
{
	int x;
	int y;
	int h;

	h = s + 1;
	x = ((l - (s - 1)) / 2) - 1;
	y = ((l + (s - 1)) / 2) - 1;
	if (s % 2 == 0 && s >= 6 && j == 1 + (s / 2) && i == y - 2)
		write(1, "$", 1);
	else if (s % 2 != 0 && s >= 6 && j == 2 + (s / 2) && i == y - 2)
		write(1, "$", 1);
	else if (s % 2 == 0 && j >= h - (s - 1) + 1 - 1 && x <= i && i < y)
		write(1, "|", 1);
	else if (s % 2 != 0 && j >= h - (s - 1) + 1 && x + 1 <= i && i < y)
		write(1, "|", 1);
	else
		write(1, "*", 1);
}

void	affiche_dernieretage(int h, int l, int d, int s)
{
	int	i;
	int j;

	j = 0;
	s++;
	while (j < h)
	{
		i = -1 - d;
		while (i++ < h - 2 - j)
			write(1, " ", 1);
		write(1, "/", 1);
		i--;
		while (i++ < (l + j - h - 2))
			ecrire(l, s, j, i);
		write(1, "\\", 1);
		write(1, "\n", 1);
		j++;
	}
}

void	sastantua(int size)
{
	int		hauteur;
	int		largeur;
	int		j;
	int		decal;

	j = 0;
	while (j < size)
	{
		hauteur = j + 3;
		decal = (donne_largeur(size - 1) - donne_largeur(j)) / 2;
		largeur = donne_largeur(j);
		if (j == (size - 1))
			affiche_dernieretage(hauteur, largeur, decal, size);
		else
			affiche_etage(hauteur, largeur, decal);
		j++;
	}
}
